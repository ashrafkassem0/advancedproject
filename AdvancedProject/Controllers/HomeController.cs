﻿using AdvancedProject.ApplicationServices.SystemApp;
using AdvancedProject.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdvancedProject.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<EmployeeModel> employeeApp = new EmployeeApp().GetEmployees();
            return View(employeeApp);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}