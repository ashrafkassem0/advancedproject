﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedProject.Model.ViewModel
{
   public class EmployeeModel
    {
        public EmployeeModel() { }
        public EmployeeModel(Employee employee)
        {
            Id = employee.Id;
            Name = employee.Name;
            Age = employee.Age;
            Salary = employee.Salary;
            Email = employee.Email;
            CompanyId = employee.CompanyId;
            if(employee.Company!=null)
              CompanyName = employee.Company.Name;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public decimal Salary { get; set; }
        public string Email { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }

        public Employee getEmployee(EmployeeModel employee)
        {
            return new Employee {
                Age=employee.Age,
                CompanyId=employee.CompanyId,
                Email=employee.Email,
                Name=employee.Name,
                Salary=employee.Salary
            };
        }
    }
}
