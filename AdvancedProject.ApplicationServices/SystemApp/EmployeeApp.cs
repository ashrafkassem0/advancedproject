﻿using AdvancedProject.DataAccess.UnitOfWork;
using AdvancedProject.Model;
using AdvancedProject.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedProject.ApplicationServices.SystemApp
{
   public class EmployeeApp
    {
        private UnitOfWork unitOfWork;
        public EmployeeApp()
        {
            unitOfWork = new UnitOfWork();
        }

        public List<EmployeeModel> GetEmployees()
        {
            var employeesInfo= unitOfWork.Employees.GetAll().ToList();
            return employeesInfo.Select(employee => new EmployeeModel(employee)).ToList();
        }
        public EmployeeModel GetEmployee(int id)
        {
            var employee= unitOfWork.Employees.Find(id);
            return new EmployeeModel(employee);
        }
        public bool AddOrUpdateEmployee(EmployeeModel employee)
        {
            if (employee.Id == 0)
            {
                unitOfWork.Employees.Add(new EmployeeModel().getEmployee(employee));
            }
            else
            {
                var oldEmployee = unitOfWork.Employees.Find(employee.Id);
                oldEmployee.Name = employee.Name;
                oldEmployee.Salary = employee.Salary;
                oldEmployee.Age = employee.Age;
                oldEmployee.CompanyId = employee.CompanyId;
                oldEmployee.Email = employee.Email;
                unitOfWork.Employees.AddOrUpdate(oldEmployee);
            }
          return  unitOfWork.Save()>0;
        }
        public bool DeleteEmployee(int id)
        {
            unitOfWork.Employees.Remove(id);
            return unitOfWork.Save() > 0;
        }
    }
}
