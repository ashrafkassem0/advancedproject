namespace AdvancedProject.DataAccess.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AdvancedProject.DataAccess.AdvancedDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(AdvancedProject.DataAccess.AdvancedDBContext context)
        {
            if (!context.Companies.Any(c => c.Name == "admin"))
            {
                context.Companies.Add(new Model.Company
                {
                    Name = "admin",
                    Address = "asd"
                });
                context.SaveChanges();
            }

        }
    }
}
