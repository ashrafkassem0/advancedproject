﻿using AdvancedProject.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedProject.DataAccess
{
   public class AdvancedDBContext:DbContext
    {
        public AdvancedDBContext() : base("DefaultConnection")
        {

        }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Company> Companies { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<System.Data.Entity.ModelConfiguration.Conventions.OneToManyCascadeDeleteConvention>();
            //Database.SetInitializer<AdvancedDBContext>(null);//Not in First Migration 
            base.OnModelCreating(modelBuilder);
        }
        //design pattern singleTon
        public static AdvancedDBContext Create()
        {
            return new AdvancedDBContext();
        }
    }
}
