﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedProject.DataAccess.Repository
{
   public class Repository<TEntity> : IRepository<TEntity> where TEntity : class 
    {
        DbContext Context;
        public Repository(DbContext dBContext)
        {
            Context = dBContext;
        }
        public TEntity Find(object id)
        {
            return Context.Set<TEntity>().Find(id);
        }

        public TEntity SingleOrDefault(Expression<Func<TEntity, bool>> filter = null, params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> query = Context.Set<TEntity>();

            query = includes.Aggregate(query, (current, include) => current.Include(include));

            return filter != null ? query.SingleOrDefault(filter) : query.SingleOrDefault();
        }
        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> filter = null, params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> query = Context.Set<TEntity>();

            query = includes.Aggregate(query, (current, include) => current.Include(include));

            return filter != null ? query.FirstOrDefault(filter) : query.FirstOrDefault();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return Context.Set<TEntity>().AsEnumerable();
        }

        public IEnumerable<TEntity> GetAll(params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> query = Context.Set<TEntity>();
            query = includes.Aggregate(query, (current, include) => current.Include(include));

            return query.AsEnumerable();
        }
        public bool Any(Expression<Func<TEntity, bool>> predicate)
        {
            return Context.Set<TEntity>().Any(predicate);
        }
        public bool Any()
        {
            return Context.Set<TEntity>().Any();
        }
        public IQueryable<TEntity> GetQueryable()
        {
            return Context.Set<TEntity>().AsQueryable();
        }

        public IEnumerable<TEntity> PageAll(int skip, int take)
        {
            return Context.Set<TEntity>().Skip(skip).Take(take);
        }
        public IEnumerable<TEntity> PageAll(int skip, int take, Expression<Func<TEntity, bool>> predicate)
        {
            return Context.Set<TEntity>().Where(predicate).Skip(skip).Take(take);
        }
        public IEnumerable<TEntity> PageAll(int skip, int take, params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> query = Context.Set<TEntity>();
            query = includes.Aggregate(query, (current, include) => current.Include(include));

            return query.Skip(skip).Take(take).AsEnumerable();
        }
        public IEnumerable<TEntity> PageAll(int skip, int take, Expression<Func<TEntity, bool>> filter = null, params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> query = Context.Set<TEntity>();

            query = includes.Aggregate(query, (current, include) => current.Include(include));

            if (filter != null)
                query = query.Where(filter);

            return query.Skip(skip).Take(take).AsEnumerable();
        }

        public IEnumerable<TEntity> Where(Expression<Func<TEntity, bool>> predicate)
        {
            return Context.Set<TEntity>().Where(predicate);
        }

        public IEnumerable<TEntity> Where(Expression<Func<TEntity, bool>> filter = null, params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> query = Context.Set<TEntity>();

            query = includes.Aggregate(query, (current, include) => current.Include(include));

            if (filter != null)
                query = query.Where(filter);

            return query.AsEnumerable();
        }

        public IEnumerable<TEntity> Where(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> query = Context.Set<TEntity>();

            query = includes.Aggregate(query, (current, include) => current.Include(include));

            if (filter != null)
                query = query.Where(filter);

            if (orderBy != null)
                query = orderBy(query);

            return query.AsEnumerable();
        }

        public int Max(Func<TEntity, int> selector)
        {
            if (Context.Set<TEntity>().Any())
            {
                return Context.Set<TEntity>().Max(selector);
            }
            else
            {
                return 0;
            }

        }
        public long Max(Func<TEntity, long> selector)
        {
            if (Context.Set<TEntity>().Any())
            {
                return Context.Set<TEntity>().Max(selector);
            }
            else
            {
                return 0;
            }
        }

        public IEnumerable<TEntity> Include(Expression<Func<TEntity, object>> predicate)
        {
            IQueryable<TEntity> query = Context.Set<TEntity>();
            return query.Include(predicate);
        }
        public IEnumerable<TEntity> IncludeMultiple(Expression<Func<TEntity, bool>> wherePredicate, params Expression<Func<TEntity, object>>[] includeProperties)

        {
            IQueryable<TEntity> query = Context.Set<TEntity>();
            if (includeProperties != null)
            {

                foreach (var item in includeProperties)
                {
                    query.Include(item);
                }

                return query;

            }

            return query;
        }

        public IEnumerable<TEntity> IncludeMultiple(params Expression<Func<TEntity, object>>[] includeProperties)

        {
            IQueryable<TEntity> query = Context.Set<TEntity>();
            if (includeProperties != null)
            {

                foreach (var item in includeProperties)
                {
                    query.Include(item);
                }

                return query;

            }

            return query;
        }




        public int Count()
        {
            return Context.Set<TEntity>().Count();
        }

        public int Count(Expression<Func<TEntity, bool>> predicate)
        {
            return Context.Set<TEntity>().Count(predicate);
        }

        public TEntity Add(TEntity entity)
        {
            return Context.Set<TEntity>().Add(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            Context.Set<TEntity>().AddRange(entities);
        }


        public void Remove(object id)
        {
            var entity = Find(id);
            if (entity != null)
                Context.Set<TEntity>().Remove(entity);
        }

        public void Remove(TEntity entity)
        {
            Context.Set<TEntity>().Remove(entity);
        }
        public void AddOrUpdate(TEntity entity)
        {
            Context.Set<TEntity>().AddOrUpdate(entity);
        }
        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            Context.Set<TEntity>().RemoveRange(entities);
        }
    }
}
