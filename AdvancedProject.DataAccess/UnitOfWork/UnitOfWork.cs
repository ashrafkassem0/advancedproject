﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdvancedProject.DataAccess.Repository;
using AdvancedProject.Model;

namespace AdvancedProject.DataAccess.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        AdvancedDBContext context;
        public UnitOfWork()
        {
            context = AdvancedDBContext.Create();
        }
        private Repository<Company> _compuny;
        public Repository<Company> Companies {
            get => _compuny??new Repository<Company>(context);
        }
        private Repository<Employee> _Employees;
        public Repository<Employee> Employees {
            get => _Employees??new Repository<Employee>(context);
        }

        public int Save()
        {
            return context.SaveChanges();
        }
    }
}
