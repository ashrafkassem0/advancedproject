﻿using AdvancedProject.DataAccess.Repository;
using AdvancedProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedProject.DataAccess.UnitOfWork
{
   public interface IUnitOfWork
    {
         Repository<Company> Companies { get; }
         Repository<Employee> Employees { get; }

        int Save();
    }
}
